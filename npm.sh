#!/bin/bash

# Type `git open` to open the GitHub page or website for a repository.
npm install -g git-open
# trash as the safe `rm` alternative
npm install --global trash-cli

#dev
npm install -g grunt-cli
npm install -g gulp
npm install -g bower