#!/bin/bash

# Maintain cask
#     brew update && brew upgrade brew-cask && brew cleanup && brew cask cleanup` 

# Install native apps
brew install caskroom/cask/brew-cask
brew tap caskroom/versions

# daily
brew cask install dropbox
brew cask install 1password
brew cask install rescuetime
brew cask install flux

# dev
brew cask install iterm2
brew cask install sublime-text3
brew cask install sourcetree
brew cask install imagealpha
brew cask install imageoptim
brew cask install transmit
brew cask install sequel-pro
brew cask install beyond-compare
brew cask install java

# dev env
brew cask install virtualbox
brew cask install dockertoolbox
brew cask install otto
brew cask install vagrant
brew cask install vagrant-manager

# browsers
brew cask install google-chrome
brew cask install google-chrome-canary
brew cask install firefox-nightly
brew cask install webkit-nightly
