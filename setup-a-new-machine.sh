# Don't run this file as a standalone, copy and paste as needed.
  echo "Do not run this script in one go. Hit ctrl-c NOW!"
  read -n 1

# NEW MACHINE SETUP

# Install Command Line Tools

# Install Homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Homebrew forumlas
./brew.sh
# Install apps via Homebrew Cask
./brew-cask.sh
# Install CLI apps via NPM
./npm.sh

# github.com/rupa/z
git clone https://github.com/rupa/z.git ~/code/z
chmod +x ~/code/z/z.sh

# composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# ruby
rbenv install 2.2.3
rbenv global 2.2.3

# Set up osx defaults
sh .osx

# Move git credentials into ~/.gitconfig.local

# Setup symlinks
./symlink-setup.sh
